import * as firebase_functions from './firebase';
import * as display_functions from './image_loading';
import * as loginValidation_functions from './validation';
import * as boards_functions from './boards_loading';

export {
    firebase_functions,
    display_functions,
    loginValidation_functions,
    boards_functions
}

/* import authFunctions from './authentication';

var config = {
    apiKey: "AIzaSyCV-fEgoD_Eqvfh7R53iabFAypqbKS1Kug",
    authDomain: "smartpin-team1.firebaseapp.com",
    databaseURL: "https://smartpin-team1.firebaseio.com",
    projectId: "smartpin-team1",
    storageBucket: "smartpin-team1.appspot.com",
    messagingSenderId: "183920251372"
};
firebase.initializeApp(config);

function googleLogin(){
    authFunctions.googleLogin();
}
 */