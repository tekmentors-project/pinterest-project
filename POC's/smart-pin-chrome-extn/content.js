let tempImages = document.getElementsByTagName('img');
let images = [];

/* var script = document.createElement('script'); 
script.src = "https://code.jquery.com/jquery-3.3.1.min.js";
script.integrity = "sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=";
script.crossorigin="anonymous";
document.getElementsByTagName('head')[0].appendChild(script);  */

var user = localStorage.getItem('smartpin_details');

if(user){
  var uid = JSON.parse(user).uid;
  chrome.storage.local.set({
    'userid': uid
  }, function() {
    console.log('Value is set to ' + uid);
  });
}


chrome.runtime.onMessage.addListener(sendData);

function sendData(message, sender, sendResponse) {
  images = [];
  if (message.type === "get_images") {
    let viewport_height = $(window).height();
    let viewport_width = $(window).width();
    for (const image of tempImages) {
      if (image.width >= 100 && image.height >= 100) {
        images.push({
          imageLink: image.src,
          width: image.width,
          height: image.height,
          aspect_ratio: (image.width / image.height)
        });
      }
    }
    sendResponse(images);
  }
}



/* chrome.runtime.sendMessage({
  images,
  viewport_width,
  viewport_height
}); */

/* window.addEventListener('mouseup', wordSelected);

function wordSelected() {
  let selectedText = window.getSelection().toString().trim();
  console.log(selectedText);
  if (selectedText.length > 0) {
    let message = {
      text: selectedText
    };
    chrome.runtime.sendMessage(message);
  }
} */
