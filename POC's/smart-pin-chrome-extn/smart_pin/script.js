let imageMap = {};
let uid;

document.addEventListener('DOMContentLoaded', function () {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { type: "get_images" }, function (imageData) {
            $(".image-container").click(function (e) {
                $(".image-container>img#" + e.target.id).toggleClass("selected");
            });
            initialize(imageData);
        });
    });
});

function initialize(data) {
    let container = $('.image-container');

    if (data) {
        data.forEach((image, index) => {
            let imageId = "image" + (index + 1);

            imageMap[imageId] = image.imageLink;

            let imageBox = prepareImageBox(image.imageLink, imageId, 150, 150, image);
            container.append(imageBox);
        });
    }

    chrome.storage.local.get(['userid'], function (result) {
        var selectBox = $('#boards-dropdown');
        if (result) {
            uid = result.userid;
            $.ajax({
                url: "https://us-central1-smartpin-team1.cloudfunctions.net/getUserBoards?uid=" + result.userid,
                async: true,
                dataType: 'json',
                crossDomain: true,
                success: function (boards) {
                    for (key of Object.keys(boards)) {
                        selectBox.append(`<option class=optionVal value=${key}>${key.toUpperCase()}</option>`);
                    }
                }
            });
        }
    });
}

function boardSelected(event) {
    console.log(event);
}

function prepareImageBox(src, id, width, height, data) {
    var imgTag = createImgTag(src, id, width, height, data);
    return imgTag;
}


/* 
    This function is used to create the <img> tag dynamically.
*/
function createImgTag(src, id, width, height, data) {

    var imgTag = $('<img />', {
        src: src,
        alt: 'My Image',
        id: id,
        'data-width': data.width,
        'data-height': data.height,
        'data-aspectratio': data.aspect_ratio
    });
    imgTag.css('width', width);
    imgTag.css('height', height);
    return imgTag;
}

window.onload = function () {
    let boardsOption = document.getElementById('boards-dropdown');
    let addImageButton = document.getElementById('add-images');
    boardsOption.onchange = function (event) {
        if (boardsOption.value === 'create-new-board') {
            document.getElementById('new-board').removeAttribute('hidden');
        } else {
            document.getElementById('new-board').setAttribute('hidden', true);
        }
    }

    addImageButton.onclick = function () {
        let boardsOption = document.getElementById('boards-dropdown');
        var selectedImages = $('.image-container .selected');
        if (boardsOption.value === 'create-new-board') {
            let newBoard = document.getElementById('new-board').value;

            if (checkBoardValid(newBoard)) {
                addNewBoard(newBoard, uid).then(msg => {
                    let length = selectedImages.length;
                    let j = 0;
                    while (j < length) {
                        let newUrl = selectedImages[j].src;
                        let tags = [];
                        let title = 'This is title';
                        let description = 'This is description';
                        let boardName = newBoard;

                        let imgDimensions = {
                            width: selectedImages[j].dataset.width,
                            height: selectedImages[j].dataset.height,
                            aspect_ratio: selectedImages[j].dataset.aspectratio,
                        }
                        let newImage = addNewImage(newUrl, tags, uid, title, description, boardName, imgDimensions);
                        j++;
                    }
                });
            }
        } else {
            let length = selectedImages.length;
            let j = 0;
            while (j < length) {
                let newUrl = selectedImages[j].src;
                let tags = [];
                let title = 'This is title';
                let description = 'This is description';
                let boardName = boardsOption.value;

                let imgDimensions = {
                    width: selectedImages[j].dataset.width,
                    height: selectedImages[j].dataset.height,
                    aspect_ratio: selectedImages[j].dataset.aspectratio,
                }
                let newImage = addNewImage(newUrl, tags, uid, title, description, boardName, imgDimensions);
                j++;
            }
        }
    }
}

/* 
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAeFBMVEX///8dHRsAAAAZGRcuLi3Ly8sbGxnOzs4VFROpqakQEA1paWi1tbXBwcHx8fHp6elgYGA5OTcHBwCdnZzm5uba2trU1NS6urlWVlWjo6KKioomJiTz8/Pc3NxjY2JMTEt/f3+UlJRERENwcG9QUE97e3oxMS+Ojo28MmnZAAADIElEQVR4nO3ZaXuiMBSGYQyIELcWWrU4Vu0y8///4SAlgEoW7SLMPPe3enF6eCXNQj0PAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/7nNMk7TePrwM92Wg0BDznQ1sbYm8O0Nn0XlfWG+9nWtbVRYB7+s/abC1xH6hIYiS784EXKghFJsjQ/yOdE2OpCJ9hYrQzHQMiXUFxnbze5FeHx9JFLD9WPZ3qWs/WTAb0i4EVFLxfuVCX0xtwZcmAJ+fcK58FtL9lcl9IV9plqcDplvThi0PMGiRjvfGBKGDgEfzQG/POFTXSXzeTSpHqgfXJ4wFBtrwIVoUIVJ80N7QnFO31CGVZNxOpn8WVddxVRTctd2j1Hx06M1oDesjdTXm+xHjY+1pSqhWDYvtxRVf/QiKz+ZqE/ks66meZPl1dHq8Omlu4VFdctOl1cJ7ZNZTX2LjYmlDu3yC8qL5fiCphX1/YiJ0+VXJcySsqixDdkJWXCYNfqUsDGQR6u7wuqfSJgKNQu6NTnT+YTV/ikUMrPsuFvdJOHyeB4dme+wWn7DfFEax5dOh7dIeLoero1VabWiFTeaL3CZfm1pcZOEx/x7c9nLyQ7Fzx9l5j7Qe5DQ+31Wl4/XV/sh9kMfEnrjltOFdNxm9COhNwxajojGQ3CtHwk9b7oS4vTEoN15H+n+XKrM4sORofkow+7uSyfTY84NF2meMqkf4pNDzU0S2t8EmZruquEavThc3/Vd20wN5ea08nrJ8anrCdWmLTqajt4i99HQ+YSB3zJxXjLeO59QnQ/DZtW77PgoXY5OObyn8eszsDpR+QOHrl1YD83v2l7UEhiKbfw4mz1Mq4km2Tl07cCexjLcGm/YP14I1nsbl3eDPUjo7XRlcuXStQcJvW3SWuMLpwNUHxJ6b22FkcML+oNeJPSy88OTWDu+sPlkwrDgnDDUsS1sm61IGv8Qyqcc/f/WTpRNr00YFZwTRjr2pXuThfXasordb7JsmlyV8IfNh3G63z8tXdYIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9MJfebspKLXeD+8AAAAASUVORK5CYII=" 
alt="My Image" 
id="image1" 
class="selected" 
style="width: 150px; height: 150px;"></img> */
