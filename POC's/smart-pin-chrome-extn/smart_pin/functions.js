var config = {
    apiKey: "AIzaSyCV-fEgoD_Eqvfh7R53iabFAypqbKS1Kug",
    authDomain: "smartpin-team1.firebaseapp.com",
    databaseURL: "https://smartpin-team1.firebaseio.com",
    projectId: "smartpin-team1",
    storageBucket: "smartpin-team1.appspot.com",
    messagingSenderId: "183920251372"
};
firebase.initializeApp(config);

let pName;

function addRecord(db_name, id, data) {
    var uid = firebase.database().ref().child(db_name).push().key;
    var updates = {};
    firebase.database().ref('/' + db_name + '/' + uid).set(data);
    return uid;
}

function getAllRecords(db_name) {
    return firebase.database().ref('/' + db_name + '/')
        .once('value');
}

function updateRecord(db_name, id, data) {
    var updates = {};
    updates['/' + db_name + '/' + id] = data;
    return firebase.database().ref().update(updates);
}


function getImage(imageId) {
    return firebase.database().ref('/images/' + imageId)
        .once('value');
}

function getUserBoards(uid) {
    return firebase.database().ref('boards')
        .orderByChild('uid')
        .equalTo(uid)
        .once('value');
}

function getUserImages(uid) {
    return firebase.database().ref('images')
        .orderByChild('uid')
        .equalTo(uid)
        .once('value');
}

function getBoardImages(boardKey) {
    return firebase.database().ref('/images_boards/' + boardKey)
        .once('value');
}

function signup(displayName, email, password) {
    pName = displayName;
    const auth = firebase.auth();
    let user = null;
    //console.log(email);
    auth.createUserWithEmailAndPassword(email, password)
        .then(function () {
            user = firebase.auth().currentUser;
            user.updateProfile({
                displayName: displayName,
                photoURL: ''
            });
        })
        .catch(function (error) {
            console.log(error.message);
        });
}

function signin(email, password) {
    const auth = firebase.auth();
    const promise = auth.signInWithEmailAndPassword(email, password);
    promise.catch(e => console.log(e.message));
}


function signOut() {
    firebase.auth().signOut().then(function () {
        localStorage.removeItem('smartpin_details');
        $(location).attr('href', "../index.html");
    }).catch(function (error) {
        // An error happened.

    });
}

function addNewImage(url, tags, uid, title, description, boardName,  imgDimensions) {
    return new Promise((resolve, reject) => {
        let imgData = {
            tags: tags,
            uid: uid,
            title: title,
            description: description,
            dateCreated: getCurrentDateTime(),
            width: imgDimensions.width,
            height: imgDimensions.height,
            aspect_ratio: imgDimensions.aspect_ratio,
            urls: {
                full: url,
                raw: url,
                regular: url,
                small: url,
                thumb: url
            }
        }
        let imageId = addRecord('images', '', imgData);
        imgData.imageId = imageId;
        saveImageToBoard(boardName, imgData);
        resolve('Image created successfully ');
    });
}

function saveImageToBoard(boardName, imgData) {
    addRecord('images_boards/' + boardName, '', imgData);
}

function addNewBoard(boardName, uid) {
    return new Promise((resolve, reject) => {
        checkBoardValid(boardName).then(result => {
            if (result) {
                updateRecord('boards', boardName, {
                    uid: uid,
                    dateCreated: getCurrentDateTime(),
                    secrecy: "public"
                });
                resolve('Board created successfully');
            } else {
                resolve('Board already present');
            }
        });
    });
}

function checkBoardValid(boardKey) {
    return new Promise((resolve, reject) => {
        firebase.database().ref('/boards/' + boardKey)
            .once('value').then(snapshot => {
                if (snapshot.val()) {
                    resolve(false);
                } else {
                    resolve(true);
                }
            });
    });
}

function getCurrentDateTime() {
    let date = new Date();
    let year = date.getFullYear().toString();
    let month = date.getMonth().toString();
    let day = date.getDate().toString();
    let hours = date.getHours().toString();
    let minutes = date.getMinutes().toString();
    let seconds = date.getSeconds().toString();
    let timezone = date.getTimezoneOffset().toString();
    return `${year}-${month}-${day}T${hours}:${minutes}:${seconds}${timezone}`;
}

function googleLogin() {
    let provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithRedirect(provider);
    firebase.auth().getRedirectResult().then(function (result) {
        if (result.credential) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // ...
        }
        // The signed-in user info.
        var user = result.user;

        //localStorage.setItem('myPage.expectSignIn', '1');
        /* localStorage.setItem('user', user.email);
        $(location).attr('href', "landing/landing.html"); */
    }).catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        // ...
    });
}

function onAuthEvent() {
    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            if (user.displayName === null) {
                user.updateProfile({
                    displayName: pName,
                    photoURL: ""
                }).then(function () {

                }).catch(function (error) {

                });
            }
            var displayName = user.displayName || pName;
            var email = user.email;
            var emailVerified = user.emailVerified;
            var photoURL = user.photoURL;
            var isAnonymous = user.isAnonymous;
            var uid = user.uid;
            var providerData = user.providerData;
            var obj = {
                uid: uid,
                email: email,
                name: displayName,
                photo: photoURL
            };

            localStorage.setItem("smartpin_details", JSON.stringify(obj));
            $(location).attr("href", "landing/landing.html");
        } else {
            console.log('not logged in');
            localStorage.removeItem('smartpin.user');
        }
    });
}