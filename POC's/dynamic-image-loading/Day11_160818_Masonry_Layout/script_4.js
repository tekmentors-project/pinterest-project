
var imagesJsonPath = "https://api.myjson.com/bins/gprrc";

var viewport_height;
var viewport_width;

var container_padding = 15;
var image_padding = 15;

var images = getJsonFromLink();
var image_dimensions = [];
var image_positions = [];

/* 
    Belew code is used to get the image dimensions in
    the array 'image_dimensions' one by one.
*/
images.forEach(image => {
    getImgAspectRatio(image).then(image_dimension => {
        image_dimensions.push(image_dimension);
    });
});

window.onload = function () {
    createLayout();
}

$('#image-placeholder').scroll(function() {  
    $("article").empty();
    createLayout();
});

/* 
    Below code is used to recreate the layout 
    when the viewport size is changed.
*/
window.onresize = function () {
    $("article").empty();
    createLayout();
}

/* 
    This function is used to lay the images in the layout.
*/
function createLayout() {
    viewport_height = $(window).height();
    viewport_width = $(window).width();
    calculateImagePositions(getNoColumns(viewport_width), container_padding, image_padding);
}

/* 
    This function is used to decide the number of columns in the layout 
    based on the viewport width.
    This is used to make the layout responsive.
*/
function getNoColumns(width) {
    if (width < 576) {        //condition for small sized screens.
        return 2;
    } else if (width >= 576 && width < 768) {     //condition for medium sized screens.
        return 3;
    } else if (width >= 768 && width < 992) {   //condition for large sized screens.
        return 4;
    } else {
        return 5;
    }
}

function calculateImagePositions(no_columns, container_padding, image_padding){
    let image_width = 0;
    let column_positions = [];
    let image_positions = [];

    image_width =
        (viewport_width - ((container_padding * 2) + (no_columns * image_padding * 2))) / no_columns;

    for (i = 0; i < no_columns; i++) {
        column_positions.push({
            top: 0,
            left: ((image_width * i) + ((container_padding + image_padding) * i))
        });
    }

    for (let i = 0; i < images.length; i++) {
        let image = images[i];
        let aspect_ratio = image_dimensions.find(o => o.name === image).aspect_ratio;
        
        // Picking up the position of image insertion
        // from first element in 'image_positions' array.
        let height = image_width / aspect_ratio;

        image_positions.push(Object.assign({
            imageLink: image,
            aspect_ratio: aspect_ratio,
            width: image_width
        }, column_positions[0]));

         //adding the height of the current image to the 
        column_positions[0].top += (height + 30);

        // Sorting the array 'image_positions' so that
        // the next position to use for image insertion.
        column_positions.sort((a, b) => (a.top - b.top));
    }
    //placeImages(image_positions);
    placeImages($('#image-placeholder').scrollTop(), viewport_height, image_positions, no_columns);
}

/* 
    This function is used to place images on the screen one by one.
*/
function placeImages(topPosition, viewport_height, image_positions, no_columns){
    let topImage = image_positions.find(o => o.top >= topPosition);
    let topIndex = image_positions.indexOf(topImage);
    if(topIndex >= no_columns){
        topIndex -= no_columns;
    } else{
        topIndex = 0;
    }
    
    let bottomImage = image_positions.find(o => o.top >= (topPosition+viewport_height));
    $('article').css("height", (topPosition + viewport_height));
    let bottomIndex = image_positions.indexOf(bottomImage) + 1;

    for (let i = topIndex; i < bottomIndex; i++) {
        let imageBox = prepareImageBox(image_positions[i].imageLink, 
                                       image_positions[i].width, 
                                       image_positions[i].aspect_ratio);

        // Picking up the position of image insertion
        // from first element in 'image_positions' array.
        imageBox.css("top", image_positions[i].top);
        imageBox.css("left", image_positions[i].left);
        imageBox.appendTo("article");
    }
}

/* 
    This function is used to prepare the image box by inserting the 
    image in the section tag.
*/
function prepareImageBox(src, width, aspect_ratio) {
    var imageBox = createSectionTag('image');
    var imgTag = createImgTag(src, width, (width / aspect_ratio));
    imgTag.appendTo(imageBox);
    return imageBox;
}

/* 
    This function is used to create the <img> tag dynamically.
*/
function createImgTag(src, width, height) {
    var imgTag = $('<img />', {
        src: src,
        alt: 'My Image'
    });
    imgTag.css('width', width);
    imgTag.css('height', height);
    return imgTag;
}

/* 
    This function is used to create the <section> tag dynamically.
*/
function createSectionTag(className) {
    var sectionTag = $('<section/>', {
        class: className
    });
    return sectionTag;
}

/* 
    This function is used to get the JSON file containing image links
    and return an array of images.
    This function will return an object, which has key:value pairs as,
    image-link:aspect_ratio.
*/
function getJsonFromLink() {
    var imageLinks;
    $.ajax({
        url: imagesJsonPath,
        async: false,
        dataType: 'json',
        success: function (data) {
            imageLinks = data.images;
        }
    });
    return imageLinks;
}

/* 
    This function is used to get the aspect ratio from the image link.
*/
function getImgAspectRatio(imgLink) {
    return new Promise((resolve, reject) => {
        var obj = {};
        var img = new Image();
        img.onload = () => resolve({
            name: imgLink,
            aspect_ratio: img.width / img.height
        });
        img.src = imgLink;
    });
}