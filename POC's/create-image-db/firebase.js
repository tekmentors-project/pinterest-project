var config = {
    apiKey: "AIzaSyCV-fEgoD_Eqvfh7R53iabFAypqbKS1Kug",
    authDomain: "smartpin-team1.firebaseapp.com",
    databaseURL: "https://smartpin-team1.firebaseio.com",
    projectId: "smartpin-team1",
    storageBucket: "smartpin-team1.appspot.com",
    messagingSenderId: "183920251372"
};
firebase.initializeApp(config);

function addRecord(db_name, id, data) {
    var uid = firebase.database().ref().child(db_name).push().key;
    var updates = {};
    firebase.database().ref('/' + db_name + '/' + uid).set(data);
    return uid;
}

function getAllRecords(db_name) {
    return firebase.database().ref('/' + db_name + '/')
        .once('value');
}

function updateRecord(db_name, id, data) {
    var updates = {};

    updates['/' + db_name + '/' + id] = data;
    return firebase.database().ref().update(updates);
}

function populate() {
    var imagesJsonPath;
    for (i = 1; i <= 10; i++) {
        imagesJsonPath = `https://api.unsplash.com/collections/?page=${i}&per_page=30&client_id=4e347cfdd7d752273171202c230cbeba1ca75e23112b57b3e7788b097660149a`;
        console.log(getJsonFromLink(imagesJsonPath));
    }
}

function checkBoardValid(boardKey) {
    return new Promise((resolve, reject) => {
        firebase.database().ref('/boards/' + boardKey)
            .once('value').then(snapshot => {
                if (snapshot.val()) {
                    resolve(false);
                } else {
                    resolve(true);
                }
            });
    });
}

function getJsonFromLink(imagesJsonPath) {
    var imageLinks;
    var users = ["Xyl03pAciJfu8Kmf9tsbtKalIJe2",
        "pwUyV8V2uNTP26jq0La0FXPtt3S2",
        "zcC7hBKPEwgNtzA7RAZAizoSD9j1"/*,
                 "u7J0Kr4EZieLFCQXrI7pHYgFOch1",
                 "zcC7hBKPEwgNtzA7RAZAizoSD9j1" */];

    $.ajax({
        url: imagesJsonPath,
        async: true,
        dataType: 'json',
        success: function (images) {
            images.forEach(image => {

                /* 
                    Adding images data
                */
                let tags = [];
                let uid = users[Math.floor(Math.random() * 3)];

                Object.keys(image.tags).forEach(key => {
                    tags.push(image.tags[key].title);
                });

                let imgData = {
                    tags: tags,
                    uid: uid,
                    title: image.title,
                    description: image.description,
                    dateCreated: image.published_at,
                    width: image.cover_photo.width,
                    height: image.cover_photo.height,
                    aspect_ratio: (image.cover_photo.width / image.cover_photo.height),
                    urls: {
                        full: image.cover_photo.urls.full,
                        raw: image.cover_photo.urls.raw,
                        regular: image.cover_photo.urls.regular,
                        small: image.cover_photo.urls.small,
                        thumb: image.cover_photo.urls.thumb
                    }
                };

                let imageId = addRecord('images', '', imgData);

                /* 
                    Adding boards data
                */
                image.tags.forEach(tag => {
                    var boardName = tag.title.split(' ').join('-');
                    
                    if (checkBoardValid(boardName)) {
                        updateRecord('boards', boardName, {
                            uid: uid,
                            dateCreated: image.published_at,
                            secrecy: "public"
                        });
                    }

                    imgData.imageId = imageId;
                    addRecord('images_boards/' + boardName, '', imgData);
                });
            });
        }
    });
    return imageLinks;
}