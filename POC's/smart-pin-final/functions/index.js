const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

exports.getAllImages = functions.https.onRequest((request, response) => {
    admin.database().ref('images').once('value').then(snapshot => {
        const data = snapshot.val();
        response.set("Content-type", "application/json");
        response.set("Access-Control-Allow-Origin", "*");
        response.send(data);
    });
});

exports.getImage = functions.https.onRequest((request, response) => {
    admin.database().ref('images/' + request.query.imageId).once('value').then(snapshot => {
        const data = snapshot.val();
        response.set("Content-type", "application/json");
        response.set("Access-Control-Allow-Origin", "*");
        response.send(data);
    });
});

exports.getAllBoards = functions.https.onRequest((request, response) => {
    admin.database().ref('boards').once('value').then(snapshot => {
        const data = snapshot.val();
        response.set("Content-type", "application/json");
        response.set("Access-Control-Allow-Origin", "*");
        response.send(data);
    });
});

exports.getUserBoards = functions.https.onRequest((request, response) => {
    admin.database().ref('boards')
        .orderByChild('uid')
        .equalTo(request.query.uid)
        .once('value')
        .then(snapshot => {
            const data = snapshot.val();
            response.set("Content-type", "application/json");
            response.set("Access-Control-Allow-Origin", "*");
            response.send(data);
        });
});

exports.getUserImages = functions.https.onRequest((request, response) => {
    admin.database().ref('images')
        .orderByChild('uid')
        .equalTo(request.query.uid)
        .once('value')
        .then(snapshot => {
            const data = snapshot.val();
            response.set("Content-type", "application/json");
            response.set("Access-Control-Allow-Origin", "*");
            response.send(data);
        });
});

exports.getBoardImages = functions.https.onRequest((request, response) => {
    admin.database().ref('images_boards/' + request.query.boardKey)
        .once('value')
        .then(snapshot => {
            const data = snapshot.val();
            response.set("Content-type", "application/json");
            response.set("Access-Control-Allow-Origin", "*");
            response.send(data);
        });
});

exports.checkBoardValid = functions.https.onRequest((request, response) => {
    admin.database().ref('/boards/' + request.query.boardKey)
        .once('value')
        .then(snapshot => {
            response.set("Content-type", "application/json");
            response.set("Access-Control-Allow-Origin", "*");
            if (snapshot.val()) {
                response.send({
                    result: false
                });
            } else {
                response.send({
                    result: true
                });
            }
        });
});
