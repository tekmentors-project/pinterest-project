import { firebase_functions } from ".";

export function createLayout(board_dimensions, container_padding, board_padding, viewport_width, viewport_height) {

    let no_columns = getNoColumns(viewport_width);
    let board_width =
        (viewport_width - ((container_padding * 2) + (no_columns * board_padding * 2))) / no_columns;

    let column_positions = getColumnPositions(container_padding, board_padding, board_width, no_columns);

    let board_positions = calculateImagePositions(
        board_dimensions, column_positions, board_width);
    return board_positions;
}

/* 
    This function is used to decide the number of columns in the layout 
    based on the viewport width.
    This is used to make the layout responsive.
*/
function getNoColumns(width) {
    if (width < 576) {        //condition for small sized screens.
        return 2;
    } else if (width >= 576 && width < 768) {     //condition for medium sized screens.
        return 3;
    } else if (width >= 768 && width < 992) {   //condition for large sized screens.
        return 4;
    } else {
        return 5;
    }
}

function getColumnPositions(container_padding, board_padding, board_width, no_columns) {

    let column_positions = [];

    for (let i = 0; i < no_columns; i++) {
        column_positions.push({
            top: 0,
            left: ((board_width * i) + ((container_padding + board_padding) * i)),
        });
    }

    return column_positions;
}

function calculateImagePositions(board_dimensions, column_positions, board_width) {

    let board_positions = [];
    let height = board_width;

    for (let i = 0; i < board_dimensions.length; i++) {

        board_positions.push(Object.assign({
            boardKey: board_dimensions[i].boardKey,
            width: board_width,
            height: height,
            uid: board_dimensions[i].uid,
        }, column_positions[0]));

        //adding the height of the current image to the 
        column_positions[0].top += (height + 30);

        // Sorting the array 'image_positions' so that
        // the next position to use for image insertion.
        column_positions.sort((a, b) => (a.top - b.top));
    }
    return board_positions;
}

export function placeBoards(topPosition, board_positions, viewport_height) {

    let topBoard = board_positions.find(o => o.top >= (topPosition - viewport_height));
    let topIndex = board_positions.indexOf(topBoard);

    let bottomBoard = board_positions.find(o => o.top >= (topPosition + (2 * viewport_height)));

    if (bottomBoard === undefined) {
        bottomBoard = board_positions[board_positions.length - 1];
    }
    let bottomIndex = board_positions.indexOf(bottomBoard);

    if (bottomIndex < board_positions.length) {

        $("#board-article").empty();
        /* $("#board-article").css("height", bottomBoard.top + bottomBoard.height + 30); */

        for (let i = topIndex; i <= bottomIndex; i++) {
            let boardBox = prepareBoardBox(board_positions[i].width,
                board_positions[i].height,
                board_positions[i].boardKey);

            /*// Picking up the position of image insertion
            // from first element in 'image_positions' array.
            imageBox.css("top", image_positions[i].top);
            imageBox.css("left", image_positions[i].left); */

            boardBox.css("top", board_positions[i].top);
            boardBox.css("left", board_positions[i].left);
            boardBox.css("width", board_positions[i].width);
            boardBox.css("height", board_positions[i].height);
            boardBox.appendTo("#board-article");
        }
    }
}

/* 
    This function is used to prepare the image box by inserting the 
    image in the section tag.
*/
function prepareBoardBox(width, height, boardKey) {
    var boardBox = createSectionTag('boardBox', boardKey);
    var imageBox = createSectionTag('imageBox');
    boardBox.append(imageBox);

    firebase_functions.getBoardImages(boardKey).then(snapshot1 => {
        let images = snapshot1.val();

        let keys = Object.keys(images);
        let i = 0;

        for (i = 0; i < keys.length; i++) {
            if (i === 4) {
                break;
            } else {
                imageBox.append(createImgTag(images[keys[i]].urls.small, width / 3, height / 3, 'image'));
            }
        }

        while (i < 4) {
            /* imageBox.append(createImgTag('', width / 3, height / 3, 'image')); */
            let spareImage = createSectionTag('spareImg');
            /* spareImage.css("width", width / 3);
            spareImage.css("height", height / 3);
            spareImage.css("display", block); */
            spareImage.css("flex-basis", width/3);
            imageBox.append(spareImage);
            i++;
        }

        var titleSection = createSectionTag('titleText');
        titleSection.append(`<p>${boardKey} (${keys.length})</p>`);
        boardBox.append(titleSection);
    });
    return boardBox;
}

/* 
    This function is used to create the <img> tag dynamically.
*/
function createImgTag(src, width, height, imageKey) {
    var imgTag = $('<img />', {
        src: src,
        name: imageKey
    });

    var aTag = $('<a />', {
        src: src
    });

    imgTag.css('width', width);
    imgTag.css('height', height);
    return imgTag;
}

/* 
    This function is used to create the <section> tag dynamically.
*/
function createSectionTag(className, boardKey) {
    var sectionTag = $('<section/>', {
        class: className,
        id: boardKey
    });
    return sectionTag;
}