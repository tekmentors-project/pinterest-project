export function validation(e) {
    if(e.type === "email") {
        validateEmail(e);
    }
    else if(e.type === "password") {
        validatePassword(e);
    }
    else if(e.type === "text") {
        validateProfileName(e);
    }
}

export function validateEmail(obj) {
	if(obj.value==="") {
		obj.nextElementSibling.innerText = "Email is required";
        obj.nextElementSibling.classList.remove("not-visible");
        return false;
    }
   	else if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(obj.value)))
  	{
  	obj.nextElementSibling.innerText = "Please enter valid email address";
   	obj.nextElementSibling.classList.remove("not-visible");
    return false;
  	}
       showErrorMessage(obj);
       return true;
   
}

export function validatePassword(obj) {
    if(obj.value==="") {
        obj.nextElementSibling.innerText = "Password is required";
        obj.nextElementSibling.classList.remove("not-visible");
        return false;
    }
    else if(obj.value.length < 6) {
        obj.nextElementSibling.innerText = "Minimum 6 length password is required";
        obj.nextElementSibling.classList.remove("not-visible");
        return false;
    }
    else{ 
    	showErrorMessage(obj);
        return true;
	}
    

}
export function validateProfileName(obj) {
    if(obj.value==="") {
        obj.nextElementSibling.innerText = "Profile name is required";
        obj.nextElementSibling.classList.remove("not-visible");
        return false;
    }
    else{ 
    	showErrorMessage(obj);
        return true;
	}
    

}
function showErrorMessage(obj) {
 	if(!obj.nextElementSibling.classList.contains("not-visible")){
        obj.nextElementSibling.classList.add("not-visible");
    }    
}

