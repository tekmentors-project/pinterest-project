export function createLayout(image_dimensions, container_padding, image_padding, viewport_width, viewport_height) {

    var no_columns = getNoColumns(viewport_width);
    let image_width =
        (viewport_width - ((container_padding * 2) + (no_columns * image_padding * 2))) / no_columns;

    var column_positions = getColumnPositions(container_padding, image_padding, image_width, no_columns);

    var image_positions = calculateImagePositions(
                                image_dimensions, column_positions, image_width);
    
    return image_positions;
}

/* 
    This function is used to decide the number of columns in the layout 
    based on the viewport width.
    This is used to make the layout responsive.
*/
function getNoColumns(width) {
    if (width < 576) {        //condition for small sized screens.
        return 2;
    } else if (width >= 576 && width < 768) {     //condition for medium sized screens.
        return 3;
    } else if (width >= 768 && width < 992) {   //condition for large sized screens.
        return 4;
    } else {
        return 5;
    }
}

function getColumnPositions(container_padding, image_padding, image_width, no_columns) {

    let column_positions = [];

    for (let i = 0; i < no_columns; i++) {
        column_positions.push({
            top: 0,
            left: ((image_width * i) + ((container_padding + image_padding) * i)),
        });
    }

    return column_positions;
}

function calculateImagePositions(image_dimensions, column_positions, image_width) {
    
    let image_positions = [];

    for (let i = 0; i < image_dimensions.length; i++) {
        let aspect_ratio = image_dimensions[i].aspect_ratio;

        // Picking up the position of image insertion
        // from first element in 'image_positions' array.
        let height = image_width / aspect_ratio;

        image_positions.push(Object.assign({
            imageLink: image_dimensions[i].name,
            aspect_ratio: aspect_ratio,
            width: image_width,            
            imageKey: image_dimensions[i].imageKey,
            /* bottom: column_positions[0].top + height + 30 */
        }, column_positions[0]));

        //adding the height of the current image to the 
        column_positions[0].top += (height + 30);

        // Sorting the array 'image_positions' so that
        // the next position to use for image insertion.
        column_positions.sort((a, b) => (a.top - b.top));
    }
    return image_positions;
}

export function placeImages(topPosition, image_positions, viewport_height) {
        
    let topImage = image_positions.find(o => o.top >= (topPosition - viewport_height));
    let topIndex = image_positions.indexOf(topImage);

    let bottomImage = image_positions.find(o => o.top >= (topPosition + (2 * viewport_height)));     
    
    if(bottomImage === undefined){
        bottomImage = image_positions[image_positions.length - 1];   
    }     
    let bottomIndex = image_positions.indexOf(bottomImage); 

    if (bottomIndex < image_positions.length) {        

        $("#image-article").empty();
        //$("#image-article").css("height", bottomImage.top + (bottomImage.width/bottomImage.aspect_ratio) + 30);

        for (let i = topIndex; i <= bottomIndex; i++) {
            let imageBox = prepareImageBox(image_positions[i].imageLink,
                image_positions[i].width,
                image_positions[i].aspect_ratio,
                image_positions[i].imageKey);

            // Picking up the position of image insertion
            // from first element in 'image_positions' array.
            imageBox.css("top", image_positions[i].top);
            imageBox.css("left", image_positions[i].left);
            imageBox.appendTo("#image-article");
        }
    }
}

/* 
    This function is used to prepare the image box by inserting the 
    image in the section tag.
*/
function prepareImageBox(src, width, aspect_ratio, imageKey) {
    
    var imageBox = createSectionTag('image');
    var imgTag = createImgTag(src, width, (width / aspect_ratio), imageKey);
    imgTag.appendTo(imageBox);
    return imageBox;
}

/* 
    This function is used to create the <img> tag dynamically.
*/
function createImgTag(src, width, height, imageKey) {
    var imgTag = $('<img />', {
        src: src,
        alt: 'My Image',
        id: imageKey
    });

    var aTag = $('<a />', {
        src: src,
        alt: 'My Image'
    });

    imgTag.css('width', width);
    imgTag.css('height', height);
    return imgTag;
}

/* 
    This function is used to create the <section> tag dynamically.
*/
function createSectionTag(className) {
    var sectionTag = $('<section/>', {
        class: className
    });
    return sectionTag;
}