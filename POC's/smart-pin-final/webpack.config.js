const mypath = __dirname + '/public';

module.exports = {
    mode: 'development',
    entry: {
        MyModel: './src/index.js'
    },
    output: {
        path: mypath,
        filename: 'bundle.js',
        library: 'MyModel'
    }/* ,
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    } */
}